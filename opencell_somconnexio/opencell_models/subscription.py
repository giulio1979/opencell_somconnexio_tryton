from opencell_resource import OpenCellResource


class SubscriptionFromContract(OpenCellResource):
    white_list = ['code', 'description', 'userAccount', 'offerTemplate', 'subscriptionDate']
    offer_template_code = {
        "mobile": "OF_SC_TEMPLATE_MOB",
        "internet": "OF_SC_TEMPLATE_BA"
    }

    def __init__(self, contract, crm_account_hierarchy_code):
        self.contract = contract
        self.userAccount = crm_account_hierarchy_code
        self.white_list = ['code', 'description', 'userAccount', 'offerTemplate', 'subscriptionDate']

    @property
    def code(self):
        return self.contract.id

    @property
    def description(self):
        return self.contract.msidsn

    @property
    def offerTemplate(self):
        """
        Returns offer template code for current contract's service type.

        :return: offer template code (string)
        """
        return self.offer_template_code[self.contract.service_type]

    @property
    def subscriptionDate(self):
        return self.contract.start_date.strftime("%Y-%m-%d")
