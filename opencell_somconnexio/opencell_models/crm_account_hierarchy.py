import time
import logging

from .account_hierarchy_resource import AccountHierarchyResource

logger = logging.getLogger(__name__)


class CRMAccountHierarchyFromContract(AccountHierarchyResource):
    def __init__(self, contract, crm_account_hierarchy_code):
        self.contract = contract
        self.crm_account_hierarchy_code = crm_account_hierarchy_code
        self.party = contract.party
        self.white_list = ['address', 'billingCycle', 'code', 'contactInformation', 'country', 'crmAccountType',
                           'crmParentCode', 'currency', 'customerCategory', 'description', 'electronicBilling',
                           'language', 'methodOfPayment', 'name', 'vatNo', 'email', 'mailingType', 'emailTemplate']

    @property
    def email(self):
        try:
            return self.contract.contact_email.value
        except AttributeError:
            logger.error("Contract {} not has a contact email assigned. And it is REQUIRED!".format(self.contract.id))
            return ""

    @property
    def code(self):
        return self.crm_account_hierarchy_code

    @property
    def crmAccountType(self):
        return "CA_UA"

    @property
    def phone(self):
        contact_phone = self.party.get_contact_phone()

        if contact_phone:
            return contact_phone.value
        return self.contract.msidsn

    @property
    def crmParentCode(self):
        return self.party.id

    @property
    def language(self):
        """
        A conversion has to be done:
            If tryton language code is es_ES, ESP will be passed to Opencell.
            If tryton language code is ca_ES, CAT will be passed to Opencell.
        """
        lang_code = self.party.lang.code
        if lang_code == 'es_ES':
            return 'ESP'
        elif lang_code == 'ca_ES':
            return 'CAT'
        else:
            raise Exception("Can't match tryton's lang_code {} with an OpenCell language code".format(lang_code))

    @property
    def methodOfPayment(self):
        # DTO in OC - https://api.opencellsoft.com/6.0.0/json_PaymentMethodDto.html
        if not self.contract.receivable_bank_account:
            return []
        return [{
            "paymentMethodType": "DIRECTDEBIT",
            "disabled": False,
            "preferred": True,
            "customerAccountCode": self.code,
            "bankCoordinates": {
                "iban": self._contract_iban(),
                "bic": self.contract.receivable_bank_account.bank.bic,
                "accountOwner": self.party.full_name[:self.MAX_LENGTH],
                "bankName": self.contract.receivable_bank_account.bank.rec_name,
            },
            "alias": self.contract.receivable_bank_account.id,
            "mandateIdentification": self.contract.receivable_bank_account.id,
            "mandateDate": int(time.mktime(self.contract.receivable_bank_account.create_date.timetuple())),
        }]

    def _contract_iban(self):
        try:
            return self.contract.receivable_bank_account.numbers[0].number_compact
        except IndexError:
            logger.error("Can't find iban for contract {}".format(self.contract.id))

    @property
    def customerCategory(self):
        return 'CLIENT'

    @property
    def currency(self):
        return 'EUR'

    @property
    def billingCycle(self):
        return 'BC_SC_MONTHLY_1ST'

    @property
    def country(self):
        return 'SP'

    @property
    def electronicBilling(self):
        return True

    @property
    def mailingType(self):
        return 'Manual'

    @property
    def emailTemplate(self):
        return 'EMAIL_TEMPLATE_TEST'
