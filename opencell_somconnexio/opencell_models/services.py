from .opencell_resource import OpenCellResource
from ..exceptions import OpenCellConfigurationDoesNotExist


class ServicesFromContract(OpenCellResource):
    def __init__(self, contract, opencell_configuration):
        self.contract = contract
        self.opencell_configuration = opencell_configuration

    def services_to_activate(self):
        """
        Return a list of dicts with the OpenCell service code and the start date of the contract line.

        To construct the dict, you need to get the service code form the product and the date from the
        start date of the contract line.

        :return array: List of dictionaries with the code and the start date.
        """
        services = []
        for line in self.contract.lines:
            try:
                opencell_service_dict = ContractLineToOCServiceDict(line, self.opencell_configuration).convert()
            except OpenCellConfigurationDoesNotExist:
                continue
            services.append(opencell_service_dict)
        return services


class ContractLineToOCServiceDict:
    """
    Presenter parsing OpenCell service and returning an OpenCell compliant service
    """

    def __init__(self, contract_line, opencell_configuration):
        self.contract_line = contract_line
        self.opencell_configuration = opencell_configuration

    def service_dict(self, service_code, subscription_date):
        return {
            "code": service_code,
            "quantity": 1,
            "subscriptionDate": subscription_date,
        }

    def convert(self):
        product_template_id = self.contract_line.product.template.id

        service_code = self.opencell_configuration.get_service_code_for_product_template(product_template_id)

        return self.service_dict(service_code, self.contract_line.start_date.strftime("%Y-%m-%d"))
