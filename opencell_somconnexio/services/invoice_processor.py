from decimal import Decimal
from datetime import datetime, timedelta
import logging

from trytond.pool import Pool

from account_move_invoice_creator import AccountMoveInvoiceCreatorService
from account_move_adjustment_creator import AccountMoveAdjustmentCreatorService
from ..exceptions import UnsupportedInvoiceType

logger = logging.getLogger(__name__)


class InvoiceProcessorService:
    def __init__(self, opencell_raw_invoice):
        self.opencell_raw_invoice = opencell_raw_invoice
        self.OpenCellInvoice = Pool().get('opencell.invoice')
        self.AccountMove = Pool().get('account.move')

    def process(self):
        if self._invoice_already_exists():
            logger.info("Invoice {} already processed".format(self.opencell_raw_invoice.invoiceNumber))
            return None
        if not self._invoice_is_validated():
            logger.info("Invoice ID:{} is not validated".format(self.opencell_raw_invoice.invoiceId))
            return None

        self._opencell_invoice()
        self._account_move()

    def _invoice_is_validated(self):
        """
        We want to process only the validated invoices. We haven't any other way
        to verify if the invoice has been validated, the only way is to look for
        an existing invoice number.
        In the version 7.3.0 we don't have the invoice status in the invoice.
        """
        if self.opencell_raw_invoice.invoiceNumber:
            return True
        return False

    def _invoice_already_exists(self):
        opencell_invoices = self.OpenCellInvoice.search([
            ('invoice_number', '=', self.opencell_raw_invoice.invoiceNumber),
        ])
        self.opencell_invoice = opencell_invoices[0] if opencell_invoices else None
        return opencell_invoices

    def _opencell_invoice(self):
        self.opencell_invoice = self.OpenCellInvoice(
            invoice_id=self.opencell_raw_invoice.invoiceId,
            invoice_number=self.opencell_raw_invoice.invoiceNumber,
            invoice_type=self.opencell_raw_invoice.invoiceType,
            invoice_date=self._invoice_date(),
            amount_without_tax=Decimal(str(round(self.opencell_raw_invoice.amountWithoutTax, 4))),
            tax_amount=Decimal(str(round(self.opencell_raw_invoice.amountTax, 4))),
            amount_with_tax=Decimal(str(round(self.opencell_raw_invoice.amountWithTax, 4))))
        self.opencell_invoice.save()
        logger.info("Created OpenCellInvoice number {}".format(self.opencell_raw_invoice.invoiceNumber))

    def _invoice_date(self):
        # TODO: Fix this timezone change
        # We have a problem with the timezone. The timestamp getted from the invoice polling is in UTC,
        # and we want save the local date of the invoice.
        # The invoices have the time to 00:00:00 and when OC convert this time to UTC, remove 2 hours and
        # change the day to the previous day. If we add 2 hours, the date is always the local time :D
        # from pytz import timezone
        # localtz = timezone('Europe/Madrid')
        # return localtz.localize(datetime.fromtimestamp(int(self.opencell_raw_invoice.invoiceDate)/1000))
        return (datetime.fromtimestamp(int(self.opencell_raw_invoice.invoiceDate) / 1000) + timedelta(hours=2)).date()

    def _account_move_data(self):
        if self.opencell_raw_invoice.invoiceType == "COM":
            account_move_data = AccountMoveInvoiceCreatorService(
                self.opencell_raw_invoice,
                self.opencell_invoice).create()
        elif self.opencell_raw_invoice.invoiceType == "ADJ":
            account_move_data = AccountMoveAdjustmentCreatorService(
                self.opencell_raw_invoice,
                self.opencell_invoice).create()
        else:
            raise UnsupportedInvoiceType(
                self.opencell_raw_invoice.invoiceNumber,
                self.opencell_raw_invoice.invoiceType)
        return account_move_data

    def _account_move(self):
        self.account_move = self.AccountMove(**self._account_move_data())
        self.account_move.save()
        logger.info("Created AccountMove ID {}".format(self.account_move.id))
