from ..services.invoice_processor import InvoiceProcessorService


class InvoiceListProcessorService:
    @classmethod
    def process(cls, raw_invoice_list):
        map(lambda raw_invoice: InvoiceProcessorService(raw_invoice).process(), raw_invoice_list)
