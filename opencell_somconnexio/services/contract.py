from pyopencell.resources.crm_account_hierarchy import CRMAccountHierarchy

from .subscription import SubscriptionService
from ..opencell_models.crm_account_hierarchy import CRMAccountHierarchyFromContract


class ContractService:
    """
    Manage the bussines logic of Som Connexio working with the Contract model of Tryton.
    """

    def __init__(self, contract, opencell_configuration=None):
        self.contract = contract
        self.opencell_configuration = opencell_configuration

    def update(self):
        subscription = SubscriptionService(self.contract, self.opencell_configuration).get()
        crm_account_hierarchy_from_contract = CRMAccountHierarchyFromContract(self.contract, subscription.userAccount)
        CRMAccountHierarchy.update(**crm_account_hierarchy_from_contract.to_dict())
