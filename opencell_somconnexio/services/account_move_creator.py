from decimal import Decimal
import logging

from trytond.pool import Pool

logger = logging.getLogger(__name__)


class AccountMovesCreationError(Exception):
    message = "Can't create correctly the AccountMove and his lines corresponding to the OpenCell Invoice number {}"

    def __init__(self, contract_id):
        self.message = self.message.format(contract_id)
        super(AccountMovesCreationError, self).__init__(self.message)


class AccountMoveCreatorService:
    """
    Call to create returns a dict with the data to create an AccountMove
    """

    def __init__(self, opencell_raw_invoice, opencell_invoice):
        self._local_account_move = None
        self.opencell_raw_invoice = opencell_raw_invoice
        self.opencell_invoice = opencell_invoice

        self.AccountAccount = Pool().get("account.account")
        self.AccountJournals = Pool().get("account.journal")
        self.AccountPeriod = Pool().get("account.period")
        self.Party = Pool().get("party.party")
        self.Company = Pool().get("company.company")
        self.AccountTaxLine = Pool().get("account.tax.line")
        self.OpenCellTaxCodes = Pool().get("opencell.tax_codes")

    def create(self):
        """
        * 1 accounting move to represent the invoice
          * 1 accounting move line with the total amount with tax as debit
          * 1 accounting move line for each item in the  categoryInvoiceAgregate collection, as credit
          * 1 accounting move line for each item of the taxAggregate collection as credit
        """
        logger.info("Building the data to create the AccountMove for the OpenCellInvoice Number {}.".format(
            self.opencell_invoice.invoice_number))
        return {
            "company": self._company(),
            "journal": self._journal(),
            "date": self.opencell_invoice.invoice_date,
            "origin": self.opencell_invoice,
            "lines": filter(None, self._lines()),
            "period": self._period()
        }

    @staticmethod
    def _amount(amount):
        return Decimal(str(round(amount, 2)))

    def _lines(self):
        return self._total_amount_with_tax_move_line() + \
            self._service_account_move_lines() + \
            self._tax_account_move_lines()

    def _company(self):
        """ In all cases, party with ID=1 """
        return self.Company(1)

    def _journal(self):
        """ In all cases, the journal with journal,code='REV' will be used. """
        return self.AccountJournals.search([('code', '=', 'REV')])[0]

    def _total_amount_with_tax_move_line(self):
        """ Accounting move line with the total amount with tax as debit """
        return []

    def _party(self):
        """ Party with ID=(billingAccountCode removing the _X) """
        party_id = self.opencell_raw_invoice.billingAccountCode.split('_')[0]
        return self.Party(party_id)

    def _service_account_move_line_data(self, invoice_line):
        return []

    def _service_account_move_lines(self):
        """ Accounting move line for each item in the categoryInvoiceAggregate collection, as debit """
        invoice_lines = []
        for category_invoice in self.opencell_raw_invoice.categoryInvoiceAgregate:
            invoice_lines += map(
                lambda invoice_line: self._service_account_move_line_data(invoice_line),
                category_invoice.get("subCategoryInvoiceAgregateDto")
            )
        return filter(None, invoice_lines)

    def _tax_account_move_line_data(self, tax_line):
        return {}

    def _tax_account_move_lines(self):
        """ Accounting move line for each item of the taxaggregate collection as debit """
        return map(
            lambda tax_line: self._tax_account_move_line_data(tax_line),
            self.opencell_raw_invoice.taxAggregate
        )

    def _account_account(self, account_number):
        """ Return an account_account object corresponding to the account_number. """
        try:
            account = self.AccountAccount.search([('code', '=', account_number)])[0]
        except IndexError:
            logger.error("Can't find the Account with code {}.".format(account_number))
            raise AccountMovesCreationError(self.opencell_invoice.invoice_number)
        return account

    def _period(self):
        """ Return a period of Invoice checking the InvoiceDate. """
        try:
            return self.AccountPeriod.search([
                ('start_date', '<=', self.opencell_invoice.invoice_date),
                ('end_date', '>=', self.opencell_invoice.invoice_date)])[0]
        except IndexError:
            logger.error("Can't find the Period for the date {}.".format(
                self.opencell_invoice.invoice_date.strftime("%d-%m-%Y")))
            raise AccountMovesCreationError(self.opencell_invoice.invoice_number)

    def _account_tax_lines(self, id, amount, tax_code):
        """ Return a list of AccountinTax for the AccountMoveLine. """
        return [{
            "code": tax_code,
            "tax": id,
            "amount": amount
        }]
