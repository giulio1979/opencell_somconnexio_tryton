# coding: utf-8
import logging

from trytond.pool import Pool

from pyotrs.lib import Article, Ticket

from client import OTRSClient
from client import TicketNotCreated, ErrorCreatingSession


log = logging.getLogger(__name__)


def send_opencell_error_report(contract_id, exc):
    log.error("A request to OpenCell for contract id {id} failed with this message: {msg}".format(
        id=contract_id, msg=exc.message
    ))

    contract = Pool().get('contract')(contract_id)
    title = "Una crida a OpenCell relacionat amb contract id {} ha fallat.\n".format(contract_id)
    body = "Una crida a OpenCell relacionat amb contract id {} ha fallat.  Aquest es el missatge d'error:\n{}".format(
        contract_id, exc.message)
    try:
        OTRSTicket(contract, title, body).create()
    except (ErrorCreatingSession, TicketNotCreated):
        log.error("Can't create OTRS ticket with error report.")

    raise exc


class OTRSTicket():
    def __init__(self, contract, title, body):
        self.contract = contract
        self.title = title
        self.body = body

    def create(self):
        """
        In this build, create a OTRSClient instance and send the request to create a Ticket. After update the EContract.
        This method must be the unic method exposed by this class.
        """
        try:
            OTRSClient().create_ticket(
                self._build_ticket(),
                self._build_article())
        except (ErrorCreatingSession, TicketNotCreated) as error:
            log.error("{} {}".format(self.title, error.message))
            raise error

    def _build_ticket(self):
        try:
            email = self.contract.contact_email.value
        except AttributeError:
            email = "admin@somconnexio.coop"

        return Ticket(
            {
                'Title': self.title,
                'Type': 'Sistemes',
                'Queue': u'Revisió OpenCell',
                'State': 'new',
                'Priority': '3 normal',
                'CustomerUser': email,
            })

    def _build_article(self):
        """ Return a instance of OTRS Article to create a OTRS Ticket from Eticom Contract. """
        return Article(
            {
                'Subject': self.title,
                'Body': self.body,
                'ContentType': 'text/plain; charset=utf8'
            })
