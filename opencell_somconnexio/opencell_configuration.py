from trytond.pool import Pool


class OpenCellConfigurationWrapper:

    def get_configuration(self):
        return Pool().get('opencell.configuration').get_singleton()

    def get_service_codes(self):
        return Pool().get('opencell.service_codes')()

    def get_one_shot_charge_codes(self):
        return Pool().get('opencell.one_shot_charge_codes')()


class OpenCellConfiguration:

    def __init__(self):
        self.configuration_wrapper = OpenCellConfigurationWrapper()

    @property
    def sync_to_opencell(self):
        return self.configuration_wrapper.get_configuration().sync_to_opencell

    @property
    def seller_code(self):
        return self.configuration_wrapper.get_configuration().seller_code

    @property
    def customer_category_code(self):
        return self.configuration_wrapper.get_configuration().customer_category_code

    def get_service_code_for_product_template(self, product_template_id):
        return self.configuration_wrapper.get_service_codes().get_code_for_product_template(product_template_id)

    def get_one_shot_charge_code_for_product_template(self, product_template_id):
        return self.configuration_wrapper.get_one_shot_charge_codes().get_code_for_product_template(product_template_id)
