import logging
import os

from trytond.pool import Pool
from trytond.exceptions import UserError

from celery import Celery
from celery_tryton import TrytonTask

from pyopencell.exceptions import PyOpenCellAPIException, PyOpenCellHTTPException

from ..services.subscription import SubscriptionService
from ..services.contracts_list import ContractsListService
from ..opencell_configuration import OpenCellConfiguration
from ..actions import create_contract, finish_contract

from ..otrs.ticket import send_opencell_error_report


logger = logging.getLogger(__name__)


celery = Celery('opencell_somconnexio')
try:
    celery.conf.TRYTON_DATABASE = os.environ['TRYTON_DATABASE']
except KeyError:
    logger.error("Celery for opencell_somconnexio tasks cannot be setup. Missing TRYTON_DATABASE env var")
try:
    celery.conf.TRYTON_CONFIG = os.environ['TRYTON_CONFIG']
except KeyError:
    logger.error("Celery for opencell_somconnexio tasks cannot be setup. Missing TRYTON_CONFIG env var")


def delay_task(task_func, *args, **kwargs):
    opencell_configuration = OpenCellConfiguration()
    if opencell_configuration.sync_to_opencell is False:
        logger.warning("An attempt to run task {} has been ignored".format(task_func))
        return

    logger.info("Running async task {} with the next arguments {}".format(task_func, args))
    task_func.apply_async(
        args=args,
        kwargs=kwargs,
        countdown=30,
        queue='opencell'
    )


def run_task(task_func, *args, **kwargs):
    opencell_configuration = OpenCellConfiguration()
    if opencell_configuration.sync_to_opencell is False:
        logger.warning("An attempt to run task {} has been ignored".format(task_func))
        return

    logger.info("Running task {} with args={}, kwargs={}".format(task_func, args, kwargs))
    task_func(*args, **kwargs)


@celery.task(base=TrytonTask)
def create_contract_in_opencell(contract_id, start_date=None, **context):
    try:
        create_contract(contract_id, start_date),
    except (PyOpenCellHTTPException, PyOpenCellAPIException) as exc:
        send_opencell_error_report(contract_id, exc)


@celery.task(base=TrytonTask)
def update_contracts_in_opencell(contract_ids, values, **context):
    contracts = Pool().get('contract').browse(contract_ids)
    contracts_to_update = []
    for contract in contracts:
        if contract.state == 'confirmed':
            contracts_to_update.append(contract)
        else:
            logger.warning("An attempt to create an unconfirmed contract (id={}) in opencell has been ignored".format(
                contract.id)
            )

    if not contracts_to_update:
        return

    party = contracts_to_update[0].party
    all_party_contracts = Pool().get('contract').search([
        ('party', '=', party.id),
        ('state', '=', 'confirmed')])

    ContractsListService(contracts_to_update, OpenCellConfiguration()).update(
        values,
        all_party_contracts)


@celery.task(base=TrytonTask)
def finish_contract_in_opencell(contract_id, **context):
    try:
        finish_contract(contract_id)
    except (PyOpenCellHTTPException, PyOpenCellAPIException) as exc:
        send_opencell_error_report(contract_id, exc)


@celery.task(base=TrytonTask)
def create_service_in_opencell(contract_line_id, **context):
    contract_line = Pool().get('contract.line')(contract_line_id)

    try:
        contract = contract_line.contract
    except UserError:
        logger.warning(
            "Contract line (id={}) that can't be related to a contract & has been ignored".format(
                contract_line.id)
        )
        return

    if contract.state != 'confirmed':
        logger.warning(
            "An attempt to create a service in an unconfirmed contract (id={}) in OC has been ignored".format(
                contract.id)
        )
        return

    contract_line = Pool().get('contract.line')(contract_line_id)
    product_template_id = contract_line.service.product.template.id

    try:
        SubscriptionService(contract, OpenCellConfiguration()).create_service(contract_line, product_template_id)
    except (PyOpenCellHTTPException, PyOpenCellAPIException) as exc:
        send_opencell_error_report(contract.id, exc)


@celery.task(base=TrytonTask)
def update_service_termination_date_in_opencell(contract_line_id, **context):
    contract_line = Pool().get('contract.line')(contract_line_id)

    try:
        contract = contract_line.contract
    except UserError:
        logger.warning(
            "Contract line (id={}) that can't be related to a contract & has been ignored".format(
                contract_line.id)
        )
        return

    if contract.state != 'confirmed':
        logger.warning(
            "An attempt to update termination date of unconfirmed contract (id={}) in opencell has been ignored".format(
                contract.id)
        )
        return

    product_template_id = contract_line.service.product.template.id

    try:
        SubscriptionService(contract, OpenCellConfiguration()).update_service_termination_date(
            product_template_id,
            contract_line.end_date)
    except (PyOpenCellHTTPException, PyOpenCellAPIException) as exc:
        send_opencell_error_report(contract.id, exc)
