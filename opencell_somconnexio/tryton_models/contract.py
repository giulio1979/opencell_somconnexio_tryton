from trytond.pool import PoolMeta
from ..tryton_tasks import run_task, delay_task
from ..tryton_tasks import update_contracts_in_opencell, create_contract_in_opencell, finish_contract_in_opencell


class Contract:
    __metaclass__ = PoolMeta
    __name__ = 'contract'

    @classmethod
    def _need_to_be_process_in_batch(cls, values):
        """
        The contracts need to be processed in batch if the field updated is the `contact_email`.
        """
        return bool(values.get('contact_email') or values.get('receivable_bank_account'))

    @classmethod
    def write(cls, records, values, *args):
        super(Contract, cls).write(records, values, *args)

        if values.get('state') == "confirmed":
            for contract in records:
                delay_task(create_contract_in_opencell, contract.id)

        if values.get('state') == "finished":
            for contract in records:
                delay_task(finish_contract_in_opencell, contract.id)

        if cls._need_to_be_process_in_batch(values):
            delay_task(update_contracts_in_opencell, [contract.id for contract in records], values)
