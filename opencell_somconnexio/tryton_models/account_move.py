from trytond.pool import PoolMeta


class AccountMove:
    __metaclass__ = PoolMeta
    __name__ = 'account.move'

    @classmethod
    def _get_origin(cls):
        return super(AccountMove, cls)._get_origin() + ['opencell.invoice']
