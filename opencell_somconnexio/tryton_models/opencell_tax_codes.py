from trytond.model import ModelView, ModelSQL, fields, Unique
from ..exceptions import AccountingTaxDoesNotExist


class OpenCellTaxCodes(ModelView, ModelSQL):
    """OpenCell Tax Codes mapping"""

    __name__ = 'opencell.tax_codes'

    code = fields.Char('Opencell Tax Code', required=True)
    account_tax = fields.Many2One(
        'account.tax', 'Tax', required=True)

    @classmethod
    def __setup__(cls):
        super(OpenCellTaxCodes, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints = [
            ('account_tax_unique', Unique(table, table.account_tax),
             'Account Tax must be unique'),
        ]

    @classmethod
    def get_tax_for_opencell_code(cls, opencell_tax_code):
        account_tax_code_items = cls.search([
            ('code', '=', opencell_tax_code),
        ])

        if not account_tax_code_items:
            raise AccountingTaxDoesNotExist(opencell_tax_code)

        return account_tax_code_items[0].account_tax
