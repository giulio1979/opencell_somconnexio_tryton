import logging

from trytond.wizard import Wizard, StateView, StateTransition
from trytond.wizard import Button

from trytond.modules.opencell_somconnexio.opencell_somconnexio.tryton_tasks import run_task, delay_task
from trytond.modules.opencell_somconnexio.opencell_somconnexio.tryton_tasks import create_contract_in_opencell

logger = logging.getLogger(__name__)


class OpenCellContractMigration(Wizard):
    """ OpenCell Contract Migration """
    __name__ = 'opencell.contract.migration'
    start = StateView(
        'opencell.contract.migration.start',
        'opencell_somconnexio.opencell_contract_migration_start',
        [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Continue', 'opencell_contract_migration', 'tryton-ok', default=True)
        ]
    )
    opencell_contract_migration = StateTransition()

    def transition_opencell_contract_migration(self):
        logger.info("Starting contract migration...")

        for contract in self.start.contracts:
            delay_task(create_contract_in_opencell, contract.id, self.start.activation_date),

        return 'end'
