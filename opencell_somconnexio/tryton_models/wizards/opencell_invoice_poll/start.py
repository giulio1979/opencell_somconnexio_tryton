from trytond.model import ModelView, fields


class OpenCellInvoicePollStart(ModelView):
    """ OpenCell Invoice Poll start """
    __name__ = 'opencell.invoice.poll.start'
    date = fields.Date('Filter Date')
