from trytond.pool import PoolMeta
from ..tryton_tasks import delay_task
from ..tryton_tasks import update_service_termination_date_in_opencell, create_service_in_opencell


class ContractLine:
    __metaclass__ = PoolMeta
    __name__ = 'contract.line'

    @classmethod
    def create(cls, records, *args):
        contract_lines = super(ContractLine, cls).create(records, *args)

        if not contract_lines:
            return contract_lines

        contract = contract_lines[0].contract
        if contract.state != "confirmed":
            return contract_lines

        for contract_line in contract_lines:
            delay_task(create_service_in_opencell, contract_line.id)

        return contract_lines

    @classmethod
    def write(cls, records, values, *args):
        super(ContractLine, cls).write(records, values, *args)
        for contract_line in records:
            delay_task(update_service_termination_date_in_opencell, contract_line.id)
