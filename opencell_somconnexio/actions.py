import logging

from trytond.pool import Pool

from .services.crm_account_hierarchy import CRMAccountHierarchyFromContractService
from .opencell_configuration import OpenCellConfiguration
from .presenters.contract_presenter_factory import ContractPresenterFactory
from .services.subscription import SubscriptionService

logger = logging.getLogger()


def create_contract(contract_id, start_date=None):
    """
    Create contract in OpenCell.

    :param contract_id: Tryton's contract id
    :param start_date: date (optional)
    :return:
    """
    contract = ContractPresenterFactory(contract_id, start_date).contract()
    CRMAccountHierarchyFromContractService(contract, OpenCellConfiguration()).run()
    logger.info("Created contract (id={}) in OC.".format(contract_id))


def finish_contract(contract_id):
    """
    Finish contract in OpenCell.

    :param contract_id:
    :return:
    """
    contract = Pool().get('contract')(contract_id)

    if contract.state != 'finished':
        logger.warning("An attempt to terminate an unfinished contract (id={}) in opencell has been ignored".format(
            contract_id)
        )
        return

    SubscriptionService(contract).terminate()
