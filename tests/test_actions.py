import mock
import unittest2 as unittest

from opencell_somconnexio.actions import create_contract, finish_contract
from .factories import ContractFactory


class ActionsTests(unittest.TestCase):

    @mock.patch("opencell_somconnexio.actions.ContractPresenterFactory", new_callabel=mock.MagicMock)
    @mock.patch("opencell_somconnexio.actions.CRMAccountHierarchyFromContractService")
    def test_create_contract(self, CRMAccountHierarchyFromContractServiceMock, ContractPresenterFactoryMock):  # noqa
        create_contract(mock.ANY, mock.ANY)

        ContractPresenterFactoryMock.assert_called_with(mock.ANY, mock.ANY)
        ContractPresenterFactoryMock.return_value.contract.assert_called_with()
        contract = ContractPresenterFactoryMock.return_value.contract.return_value

        CRMAccountHierarchyFromContractServiceMock.assert_called_with(contract, mock.ANY)
        CRMAccountHierarchyFromContractServiceMock.return_value.run.assert_called_with()

    @mock.patch("opencell_somconnexio.actions.Pool", return_value=mock.Mock(spec=["get"]))
    @mock.patch("opencell_somconnexio.actions.SubscriptionService")
    def test_finish_contract(self, SubscriptionServiceMock, PoolMock):
        contract = ContractFactory(state="finished")

        pool_mock_instance = PoolMock.return_value
        get_contract_method = pool_mock_instance.get.return_value
        get_contract_method.return_value = contract

        contract_id = mock.ANY
        finish_contract(contract_id)

        pool_mock_instance.get.assert_called_with("contract")
        get_contract_method.assert_called_with(contract_id)

        SubscriptionServiceMock.assert_called_with(contract)