import unittest2 as unittest

from opencell_somconnexio.tryton_models.opencell_configuration import OpenCellConfiguration


class OpenCellConfigurationModelTests(unittest.TestCase):

    def test_opencell_configuration_has_expected_attributes(self):
        self.assertTrue(hasattr(OpenCellConfiguration, "seller_code"))
        self.assertTrue(hasattr(OpenCellConfiguration, "customer_category_code"))
