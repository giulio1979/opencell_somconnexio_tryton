import mock
import unittest2 as unittest
from opencell_somconnexio.services.contracts_list import ContractsListService

from .factories import BankAccountFactory
from .factories import ContractFactory

class ContractsListServiceTests(unittest.TestCase):

    def setUp(self):
        self.opencell_configuration = mock.Mock()

    @mock.patch("opencell_somconnexio.services.contracts_list.OTRSTicket.create")
    @mock.patch("opencell_somconnexio.services.contracts_list.ContractService.update")
    def test_contracts_list_service_update_email_different_emails(
            self, contract_service_update_mock, otrs_ticket_create_mock):
        """ Check that does not call ContractService update_email if the contracts to update has different emails """
        contract_with_different_email = ContractFactory()
        contract_with_different_email.contact_email.value = 'different_email@sc.coop'
        contracts_to_update = [ContractFactory(), contract_with_different_email]
        all_party_contracts = [ContractFactory(), contract_with_different_email]

        ContractsListService(contracts_to_update, self.opencell_configuration).update(
            {'contact_email': 'hola@sc.coop'},
            all_party_contracts)
        contract_service_update_mock.assert_not_called()
        otrs_ticket_create_mock.assert_called()

    @mock.patch("opencell_somconnexio.services.contracts_list.OTRSTicket.create")
    @mock.patch("opencell_somconnexio.services.contracts_list.ContractService.update")
    def test_contracts_list_service_update_email_not_change_all_contracts(
            self, contract_service_update_mock, otrs_ticket_create_mock):
        """ Check that does not call ContractService update_email if does not want to update all the contracts """
        contracts_to_update = [ContractFactory()]
        all_party_contracts = [ContractFactory(), ContractFactory()]

        ContractsListService(contracts_to_update, self.opencell_configuration).update(
            {'contact_email': 'hola@sc.coop'},
            all_party_contracts)
        contract_service_update_mock.assert_not_called()
        otrs_ticket_create_mock.assert_called()

    @mock.patch("opencell_somconnexio.services.contracts_list.ContractService.update")
    def test_contracts_list_service_update_email(self, contract_service_update_mock):
        """ Check that ContractService update_email is called when updating all contracts' emails and all contracts had the same email. """
        contract = ContractFactory()
        contract.contact_email.value = 'email@sc.coop'
        contracts_to_update = [contract]
        all_party_contracts = [contract]

        ContractsListService(contracts_to_update, self.opencell_configuration).update(
            {'contact_email': 'hola@sc.coop'},
            all_party_contracts)
        contract_service_update_mock.assert_called()

    @mock.patch("opencell_somconnexio.services.contracts_list.ContractService.update")
    def test_contracts_list_service_update_bank_account(self, contract_service_update_mock):
        """ Check that ContractService update is called when updating all contracts' bank accounts and all contracts had the same bank account. """
        contract = ContractFactory()
        contract.receivable_bank_account = BankAccountFactory()
        contracts_to_update = [contract]
        all_party_contracts = [contract]

        ContractsListService(contracts_to_update, self.opencell_configuration).update(
            {'receivable_bank_account': BankAccountFactory()},
            all_party_contracts)
        contract_service_update_mock.assert_called()

    @mock.patch("opencell_somconnexio.services.contracts_list.OTRSTicket.create")
    @mock.patch("opencell_somconnexio.services.contracts_list.ContractService.update")
    def test_contracts_list_service_update_bank_account_different_bank_accounts(
            self, contract_service_update_email_mock, otrs_ticket_create_mock):
        """ Check that does not call ContractService update_email if the contracts to update has different emails """
        contract_with_different_bank_account = ContractFactory()
        contract_with_different_bank_account.receivable_bank_account.numbers = []
        contracts_to_update = [ContractFactory(), contract_with_different_bank_account]
        all_party_contracts = [ContractFactory(), contract_with_different_bank_account]

        ContractsListService(contracts_to_update, self.opencell_configuration).update(
            {'receivable_bank_account': BankAccountFactory()},
            all_party_contracts)
        contract_service_update_email_mock.assert_not_called()
        otrs_ticket_create_mock.assert_called()

    @mock.patch("opencell_somconnexio.services.contracts_list.OTRSTicket.create")
    @mock.patch("opencell_somconnexio.services.contracts_list.ContractService.update")
    def test_contracts_list_service_update_bank_account_not_change_all_contracts(
            self, contract_service_update_mock, otrs_ticket_create_mock):
        """ Check that does not call ContractService update_email if does not want to update all the contracts """
        contracts_to_update = [ContractFactory()]
        all_party_contracts = [ContractFactory(), ContractFactory()]

        ContractsListService(contracts_to_update, self.opencell_configuration).update(
            {'receivable_bank_account': BankAccountFactory()},
            all_party_contracts)
        contract_service_update_mock.assert_not_called()
        otrs_ticket_create_mock.assert_called()
