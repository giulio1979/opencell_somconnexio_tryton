import mock
import unittest2 as unittest

from pyotrs.lib import HTTPError, SessionNotCreated

from .factories import ContractFactory
from opencell_somconnexio.otrs.client import ErrorCreatingSession, TicketNotCreated
from opencell_somconnexio.otrs.ticket import OTRSTicket
from .settings import REQUIRED_ENVVARS


class OTRSTicketTests(unittest.TestCase):

    def setUp(self):
        self.contract = ContractFactory()

    @mock.patch.dict('os.environ', REQUIRED_ENVVARS)
    @mock.patch("opencell_somconnexio.otrs.client.Client.session_create")
    def test_create_error_creating_session(self, session_create_mock):
        """ Raise an Exception when the session can not be created """
        session_create_mock.side_effect = HTTPError('')
        with self.assertRaises(ErrorCreatingSession):
            OTRSTicket(self.contract, title='ticket_type_A', body="some body").create()

    @mock.patch.dict('os.environ', REQUIRED_ENVVARS)
    @mock.patch("opencell_somconnexio.otrs.client.Client.ticket_create")
    @mock.patch("opencell_somconnexio.otrs.client.Client.session_create")
    def test_create_error_ticket_not_created(self, session_create_mock, ticket_create_mock):
        """ Raise an Exception when the ticket can not be created """
        session_create_mock.return_value = 'done'
        ticket_create_mock.side_effect = SessionNotCreated('')

        with self.assertRaises(TicketNotCreated):
            OTRSTicket(self.contract, title='ticket_type_A', body="some body").create()

    @mock.patch.dict('os.environ', REQUIRED_ENVVARS)
    @mock.patch("opencell_somconnexio.otrs.client.Client.ticket_create")
    @mock.patch("opencell_somconnexio.otrs.client.Client.session_create")
    def test_create_ok(self, session_create_mock, ticket_create_mock):
        """ Assert that if any Exception was raised, OTRS Client ticket_create method was called once """
        OTRSTicket(self.contract, title='ticket_type_A', body="some body").create()
        ticket_create_mock.assert_called_once()
