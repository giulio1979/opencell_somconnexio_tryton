import unittest2 as unittest
from opencell_somconnexio.opencell_models.opencell_types.address import Address

from tests.factories import AddressFactory


class OpenCellAddressTypeTests(unittest.TestCase):

    def test_dict_format(self):
        tryton_address = AddressFactory()

        expected_address_format = {
            "address1": tryton_address.street,
            "zipCode": tryton_address.zip,
            "city": tryton_address.city,
            "state": tryton_address.subdivision.name,
            "country": tryton_address.country.name,
        }

        address = Address(
            address=tryton_address.street,
            zip=tryton_address.zip,
            city=tryton_address.city,
            state=tryton_address.subdivision.name,
            country=tryton_address.country.name)

        self.assertEqual(expected_address_format, address.to_dict())
