import mock
import unittest2 as unittest

from opencell_somconnexio.tryton_models.opencell_service_codes import OpenCellServiceCodes


class SuperOpenCellServicesCodesMock(mock.MagicMock):
    __setup__ = mock.MagicMock()


class TableMock(mock.MagicMock):
    product_template = 1 # fake value


class OpenCellConfigurationModelTests(unittest.TestCase):

    def test_opencell_service_codes_has_expected_attributes(self):
        self.assertTrue(hasattr(OpenCellServiceCodes, "code"))
        self.assertTrue(hasattr(OpenCellServiceCodes, "product_template"))