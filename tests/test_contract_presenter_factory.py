from datetime import datetime, timedelta
import mock
import unittest2 as unittest

from .factories import ContractFactory, ContractLineFactory

from opencell_somconnexio.presenters.contract_presenter_factory import ContractPresenterFactory


class ContractPresenterFactoryTests(unittest.TestCase):
    def setUp(self):
        self.contract_line_start_date = datetime.today().date()
        contract_line = ContractLineFactory(start_date=self.contract_line_start_date)
        self.expected_contract = ContractFactory(lines=[contract_line])

        PoolMock = mock.patch(
            "opencell_somconnexio.presenters.contract_presenter_factory.Pool",
            return_value=mock.Mock(spec=["get"])).start()

        ContractMock = mock.Mock()

        def contract_side_effect(id):
            if id == 1:
                return self.expected_contract
        ContractMock.side_effect = contract_side_effect

        def pool_side_effect(value):
            if value == "contract":
                return ContractMock
        PoolMock.return_value.get.side_effect = pool_side_effect

    def test_contract_presenter_factory_return_expected_contract(self):
        contract_id = 1
        contract = ContractPresenterFactory(contract_id, None).contract()

        self.assertEqual(contract, self.expected_contract)

    def test_contract_presenter_factory_return_expected_contract_without_change_start_date(self):
        new_start_date = datetime.today().date() - timedelta(days=5)
        contract_id = 1
        contract = ContractPresenterFactory(contract_id, new_start_date).contract()

        self.assertEqual(contract.lines[0].start_date, self.contract_line_start_date)

    def test_contract_presenter_factory_return_expected_contract_changing_start_date(self):
        new_start_date = datetime.today().date() + timedelta(days=5)
        contract_id = 1
        contract = ContractPresenterFactory(contract_id, new_start_date).contract()

        self.assertEqual(contract.lines[0].start_date, new_start_date)

    def test_contract_presenter_factory_return_expected_contract_without_old_line(self):
        contract_id = 1
        old_contract_line = ContractLineFactory()
        old_contract_line.current = mock.Mock(return_value=False)
        current_line_id = self.expected_contract.lines[0].id
        old_line_id = old_contract_line.id

        # Add an old contract line to check if the process remove it
        self.expected_contract.lines.append(old_contract_line)

        contract = ContractPresenterFactory(contract_id).contract()

        contract_lines_ids = map(lambda line: line.id, contract.lines)

        self.assertEqual(1, len(contract.lines))
        self.assertIn(current_line_id, contract_lines_ids)
        self.assertNotIn(old_line_id, contract_lines_ids)

    def test_contract_presenter_factory_return_expected_contract_with_current_and_future_lines(self):
        contract_id = 1
        new_start_date = datetime.today().date() + timedelta(days=5)
        future_contract_line = ContractLineFactory(start_date=new_start_date)
        future_contract_line.current = mock.Mock(return_value=False)
        current_line_id = self.expected_contract.lines[0].id
        future_line_id = future_contract_line.id

        # Add a future contract line to check if the process doesn't remove it
        self.expected_contract.lines.append(future_contract_line)

        contract = ContractPresenterFactory(contract_id).contract()

        contract_lines_ids = map(lambda line: line.id, contract.lines)

        self.assertEqual(2, len(contract.lines))
        self.assertIn(current_line_id, contract_lines_ids)
        self.assertIn(future_line_id, contract_lines_ids)
