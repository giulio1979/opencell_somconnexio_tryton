import factory
import mock
import unittest2 as unittest

from pyopencell.exceptions import PyOpenCellAPIException

from opencell_somconnexio.services.crm_account_hierarchy import CRMAccountHierarchyFromContractService
from opencell_somconnexio.opencell_models.customer import CustomerFromParty
from opencell_somconnexio.opencell_models.crm_account_hierarchy import CRMAccountHierarchyFromContract
from opencell_somconnexio.opencell_models.subscription import SubscriptionFromContract
from opencell_somconnexio.opencell_models.access import AccessFromContract
from opencell_somconnexio.opencell_models.services import ServicesFromContract

from .factories import ContractFactory, OpenCellServiceCodesFactory


class OpenCellCustomerResource:
    """
    Represents an OpenCell Customer Resource.
    """

    def __init__(self, code, email=None, iban=None):
        self.code = code
        self.email = email or factory.Faker("email")
        self.iban = iban or factory.Faker("iban")

    @property
    def customerAccounts(self):
        return {
            "customerAccount": [{
                "code": self.code,
                "contactInformation": {
                    "email": self.email,
                },
                "methodOfPayment": [{
                    "bankCoordinates": {
                        "iban": self.iban
                    }
                }]
            }]}


class CRMAccountHierarchyServiceTests(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.contract = ContractFactory()
        self.opencell_configuration_mock = mock.Mock()
        self.opencell_configuration_mock.get_service_code_for_product_template.return_value = OpenCellServiceCodesFactory().code

    @mock.patch("opencell_somconnexio.services.crm_account_hierarchy.logger")
    def test_call_service_with_contract_not_confirmed(self, logger_mock):
        contract = ContractFactory(state="introduced")
        CRMAccountHierarchyFromContractService(contract, self.opencell_configuration_mock).run()

        logger_mock.warning.assert_called_with("An attempt to create an unconfirmed contract (id={}) in opencell has been ignored".format(contract.id))

    @mock.patch("opencell_somconnexio.crm_account_hierarchy_strategies.Customer")
    @mock.patch("opencell_somconnexio.services.crm_account_hierarchy.Customer")
    @mock.patch("opencell_somconnexio.services.crm_account_hierarchy.CRMAccountHierarchy")
    @mock.patch("opencell_somconnexio.services.crm_account_hierarchy.Subscription")
    @mock.patch("opencell_somconnexio.services.crm_account_hierarchy.SubscriptionService")
    @mock.patch("opencell_somconnexio.services.crm_account_hierarchy.Access")
    def test_create_customer_hierarchy_in_opencell(self, AccessMock, SubscriptionServiceMock, SubscriptionMock, CRMAccountHierarchyMock, CustomerInServiceMock, CustomerInStrategiesMock):
        """ Giving no customer in Opencell, it creates whole customer hierarchy from a Contract instance """

        CustomerInStrategiesMock.get.side_effect = PyOpenCellAPIException(
            verb=mock.ANY, url=mock.ANY, status=400, body=mock.ANY)

        CRMAccountHierarchyFromContractService(self.contract, self.opencell_configuration_mock).run()

        CustomerInStrategiesMock.get.assert_called_with(self.contract.party.id)

        customer = CustomerFromParty(self.contract.party, self.opencell_configuration_mock)
        CustomerInServiceMock.create.assert_called_with(**customer.to_dict())

        crm_account_hierarchy = CRMAccountHierarchyFromContract(self.contract, "{}_0".format(customer.code))
        CRMAccountHierarchyMock.create.assert_called_with(**crm_account_hierarchy.to_dict())

        subscription = SubscriptionFromContract(self.contract, crm_account_hierarchy.code)
        SubscriptionMock.create.assert_called_with(**subscription.to_dict())

        SubscriptionServiceMock.assert_called_with(self.contract, self.opencell_configuration_mock)
        SubscriptionServiceMock.return_value.get.assert_called_with()
        subscription_instance_mock = SubscriptionServiceMock.return_value.get.return_value

        services_to_activate = ServicesFromContract(self.contract, self.opencell_configuration_mock).services_to_activate()
        subscription_instance_mock.activate.assert_called_with(services_to_activate)

        access = AccessFromContract(self.contract)
        AccessMock.create.assert_called_with(**access.to_dict())

    @mock.patch("opencell_somconnexio.crm_account_hierarchy_strategies.Customer")
    @mock.patch("opencell_somconnexio.services.crm_account_hierarchy.CRMAccountHierarchy")
    @mock.patch("opencell_somconnexio.services.crm_account_hierarchy.Subscription")
    @mock.patch("opencell_somconnexio.services.crm_account_hierarchy.SubscriptionService")
    @mock.patch("opencell_somconnexio.services.crm_account_hierarchy.Access")
    def test_create_customer_account_hierarchy_in_opencell(self, AccessMock, SubscriptionServiceMock, SubscriptionMock, CRMAccountHierarchyMock, CustomerInStrategiesMock): # noqa
        """ Giving existing customer in Opencell, it creates customer account hierarchy in OpenCell linked to that customer """
        expected_email = "expeceted@email.com"
        customer = OpenCellCustomerResource(code="{}_0".format(self.contract.party.id), email=expected_email)
        CustomerInStrategiesMock.get().customer = customer

        CRMAccountHierarchyFromContractService(self.contract, self.opencell_configuration_mock).run()

        CustomerInStrategiesMock.get.assert_called_with(self.contract.party.id)

        crm_account_hierarchy = CRMAccountHierarchyFromContract(self.contract, "{}_1".format(customer.code))
        CRMAccountHierarchyMock.create.assert_called_with(**crm_account_hierarchy.to_dict())

        subscription = SubscriptionFromContract(self.contract, crm_account_hierarchy.code)
        SubscriptionMock.create.assert_called_with(**subscription.to_dict())

        SubscriptionServiceMock.assert_called_with(self.contract, self.opencell_configuration_mock)
        SubscriptionServiceMock.return_value.get.assert_called_with()
        subscription_instance_mock = SubscriptionServiceMock.return_value.get.return_value

        services_to_activate = ServicesFromContract(self.contract, self.opencell_configuration_mock).services_to_activate()
        subscription_instance_mock.activate.assert_called_with(services_to_activate)

        access = AccessFromContract(self.contract)
        AccessMock.create.assert_called_with(**access.to_dict())


    @mock.patch("opencell_somconnexio.crm_account_hierarchy_strategies.Customer")
    @mock.patch("opencell_somconnexio.services.crm_account_hierarchy.Subscription")
    @mock.patch("opencell_somconnexio.services.crm_account_hierarchy.SubscriptionService")
    @mock.patch("opencell_somconnexio.services.crm_account_hierarchy.Access")
    def test_create_subscription_in_opencell(self, AccessMock, SubscriptionServiceMock, SubscriptionMock, CustomerInStrategiesMock):
        """ Giving existing customer in Opencell and a Tryton's contract with same iban and email, it a creates subscription in OpenCell linked to that customer """

        expected_email = "expected@email.com"
        self.contract.contact_email.value = expected_email
        expected_iban = "ES00-0000-0000-0000-0000"
        self.contract.receivable_bank_account.numbers[0].number_compact = expected_iban

        expected_customer_code = "{}_1".format(self.contract.party.id)
        customer = OpenCellCustomerResource(code=expected_customer_code, email=expected_email, iban=expected_iban)
        CustomerInStrategiesMock.get().customer = customer

        subscription_instance_mock = mock.MagicMock()
        SubscriptionMock.get().subscription = subscription_instance_mock

        CRMAccountHierarchyFromContractService(self.contract, self.opencell_configuration_mock).run()

        CustomerInStrategiesMock.get.assert_called_with(self.contract.party.id)

        subscription = SubscriptionFromContract(self.contract, expected_customer_code)
        SubscriptionMock.create.assert_called_with(**subscription.to_dict())
        SubscriptionServiceMock.assert_called_with(self.contract, self.opencell_configuration_mock)
        SubscriptionServiceMock.return_value.get.assert_called_with()

        access = AccessFromContract(self.contract)
        AccessMock.create.assert_called_with(**access.to_dict())

        services_to_activate = ServicesFromContract(self.contract, self.opencell_configuration_mock)
        subscription_instance_mock.activate.asser_called_with(services_to_activate)
