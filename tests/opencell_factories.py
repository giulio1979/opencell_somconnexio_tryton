import factory
from datetime import datetime


class PagingFactory:
    totalNumberOfRecords = 0

    def __init__(self, total_number_of_records):
        self.totalNumberOfRecords = total_number_of_records


class OpenCellInvoiceListFactory:
    invoices = []
    paging = {}

    def __init__(self, invoices=[], total_number_of_records=0):
        self.invoices = invoices
        self.paging = PagingFactory(total_number_of_records)


class OpenCellRawInvoiceFactory:
    amountWithTax = 36
    amountTax = 6
    amountWithoutTax = 30
    invoiceDate = 1565128800000
    categoryInvoiceAgregate = [
        {
            "categoryInvoiceCode": "ICAT_SUBSCRIPTION",
            "description": "Subscriptions",
            "userAccountCode": "11987_0",
            "itemNumber": 4,
            "amountWithoutTax": 30,
            "amountTax": 6,
            "amountWithTax": 36,
            "subCategoryInvoiceAgregateDto": [
                {
                    "itemNumber": 4,
                    "accountingCode": "70500020",
                    "description": "Subscription",
                    "taxCode": "TAX_HIGH",
                    "taxPercent": 21,
                    "quantity": None,
                    "amountWithoutTax": 30,
                    "amountTax": 6,
                    "amountWithTax": 36,
                    "invoiceSubCategoryCode": "ISCAT_SC_SUBSCRIPTION",
                    "userAccountCode": "11987_0",
                    "ratedTransaction": None
                }
            ]
        }]
    taxAggregate = [
        {
            "description": "IVA 21%",
            "amountWithoutTax": 30,
            "amountTax": 6,
            "amountWithTax": 36,
            "taxCode": "TAX_HIGH",
            "taxPercent": 21,
            "accountingCode": "4770021"
        }]

    def __init__(
            self,
            invoice_id=None,
            invoice_number=None,
            party_id=None,
            invoice_date="2019-04-30",
            invoice_type="COM"):
        self.invoiceId = invoice_id if invoice_id else factory.Sequence(lambda n: n)
        self.invoiceNumber = invoice_number if invoice_number else factory.Sequence(lambda n: n)
        self.billingAccountCode = "{}_0".format(party_id if party_id else factory.Sequence(lambda n: n))
        self.invoiceType = invoice_type
        # We need to multiply by 1000 because OC returns the timestamp in miliseconds.
        self.invoiceDate = (datetime.strptime(invoice_date, '%Y-%m-%d') - datetime(1970, 1, 1)).total_seconds() * 1000  # noqa
