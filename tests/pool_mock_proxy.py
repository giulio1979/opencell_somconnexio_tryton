class PoolMockProxy:
    AccountAccountMock = None
    AccountJournalMock = None
    AccountMoveMock = None
    AccountTaxLine = None
    CompanyCompanyMock = None
    OpenCellInvoiceMock = None
    OpenCellTaxCodesMock = None
    PartyPartyMock = None
    PeriodMock = None

    @classmethod
    def get(cls, model_name):
        mock_for_model = {
            "account.account": cls.AccountAccountMock,
            "account.journal": cls.AccountJournalMock,
            "account.move": cls.AccountMoveMock,
            "account.period": cls.PeriodMock,
            "account.tax.line": cls.AccountTaxLine,
            "company.company": cls.CompanyCompanyMock,
            "opencell.invoice": cls.OpenCellInvoiceMock,
            "opencell.tax_codes": cls.OpenCellTaxCodesMock,
            "party.party": cls.PartyPartyMock,
            }
        return mock_for_model[model_name]
