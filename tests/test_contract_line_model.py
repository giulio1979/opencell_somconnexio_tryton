from datetime import datetime, timedelta
import mock
import unittest2 as unittest

from opencell_somconnexio.tryton_models.contract_line import ContractLine
from opencell_somconnexio.tryton_tasks import create_service_in_opencell, update_service_termination_date_in_opencell

from .factories import ContractLineFactory, ContractFactory


class SuperContractLineMock(mock.MagicMock):
    create = mock.MagicMock()
    write = mock.MagicMock()


class ContractLineModelTests(unittest.TestCase):

    @mock.patch("opencell_somconnexio.tryton_models.contract_line.delay_task")
    @mock.patch("opencell_somconnexio.tryton_models.contract_line.super", return_value=SuperContractLineMock)
    def test_super_create_method_is_called(self, mock1, mock2):
        expected_records = [ContractLineFactory()]

        ContractLine.create(expected_records)

        SuperContractLineMock.create.assert_called_with(expected_records)

    @mock.patch("opencell_somconnexio.tryton_models.contract_line.delay_task")
    @mock.patch("opencell_somconnexio.tryton_models.contract_line.super", return_value=SuperContractLineMock)
    def test_super_write_method_is_called(self, mock1, mock2):
        expected_records = [ContractLineFactory()]
        expected_values = {}

        ContractLine.write(expected_records, expected_values)

        SuperContractLineMock.write.assert_called_with(expected_records, expected_values)

    @mock.patch("opencell_somconnexio.tryton_models.contract_line.super", return_value=SuperContractLineMock)
    @mock.patch("opencell_somconnexio.tryton_models.contract_line.delay_task")
    def test_update_update_service_termination_date_in_opencell_in_opencell_is_called_when_updating_contract_line(self, delay_task_mock, _):
        expected_records = [ContractLineFactory()]
        expected_values = {}

        ContractLine.write(expected_records, expected_values)

        delay_task_mock.assert_called_with(update_service_termination_date_in_opencell, expected_records[0].id)

    @mock.patch("opencell_somconnexio.tryton_models.contract_line.super", return_value=SuperContractLineMock)
    @mock.patch("opencell_somconnexio.tryton_models.contract_line.delay_task")
    def test_create_service_in_opencell_is_called_when_creating_contract_line(self, delay_task_mock, _):
        expected_records = [ContractLineFactory(contract=ContractFactory())]
        SuperContractLineMock.create.return_value = expected_records

        ContractLine.create(expected_records)

        delay_task_mock.assert_called_with(create_service_in_opencell, expected_records[0].id)
