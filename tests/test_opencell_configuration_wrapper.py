import mock
import unittest2 as unittest

from .factories import OpenCellConfigurationFactory
from opencell_somconnexio.opencell_configuration import OpenCellConfiguration, OpenCellConfigurationWrapper


@mock.patch("opencell_somconnexio.opencell_configuration.Pool", return_value=mock.Mock(spec=["get"]))
class OpenCellConfigurationWrapperTests(unittest.TestCase):

    def test_get_configurarion_gets_expected_tryton_model(self, Pool_mock):
        wrapper = OpenCellConfigurationWrapper()
        wrapper.get_configuration()
        Pool_mock.return_value.get.assert_called_with("opencell.configuration")
        Pool_mock.return_value.get.return_value.get_singleton.assert_called()

    def test_get_service_codes_gets_expected_tryton_model(self, Pool_mock):
        wrapper = OpenCellConfigurationWrapper()
        wrapper.get_service_codes()
        Pool_mock.return_value.get.assert_called_with("opencell.service_codes")

    def test_get_one_shot_charge_codes_gets_expected_tryton_model(self, Pool_mock):
        wrapper = OpenCellConfigurationWrapper()
        wrapper.get_one_shot_charge_codes()
        Pool_mock.return_value.get.assert_called_with("opencell.one_shot_charge_codes")


class ExpectedOpenCellConfigurationWrapper:
    def __init__(self):
        self.configuration = OpenCellConfigurationFactory()
        self.service_codes = mock.Mock()
        self.one_shot_charge_codes = mock.Mock()

    def get_configuration(self):
        return self.configuration

    def get_service_codes(self):
        return self.service_codes

    def get_one_shot_charge_codes(self):
        return self.one_shot_charge_codes


class OpenCellConfigurationTests(unittest.TestCase):

    def setUp(self):
        with mock.patch("opencell_somconnexio.opencell_configuration.OpenCellConfigurationWrapper", ExpectedOpenCellConfigurationWrapper):
            self.opencell_configuration = OpenCellConfiguration()
        self.expected_opencell_configuration_wrapper = self.opencell_configuration.configuration_wrapper

    def test_seller_code(self):
        self.assertEquals(
            self.opencell_configuration.seller_code,
            self.expected_opencell_configuration_wrapper.get_configuration().seller_code)

    def test_customer_category_code(self):
        self.assertEquals(
            self.opencell_configuration.customer_category_code,
            self.expected_opencell_configuration_wrapper.get_configuration().customer_category_code)

    def test_sync_to_opencell(self):
        self.assertEquals(
            self.opencell_configuration.sync_to_opencell,
            self.expected_opencell_configuration_wrapper.get_configuration().sync_to_opencell)

    def test_get_service_code_for_product_template(self):
        product_template_id = mock.ANY
        self.opencell_configuration.get_service_code_for_product_template(product_template_id)
        wrapper = self.expected_opencell_configuration_wrapper
        wrapper.get_service_codes().get_code_for_product_template.assert_called_with(product_template_id)

    def test_get_one_shot_code_for_product_template(self):
        product_template_id = mock.ANY
        self.opencell_configuration.get_one_shot_charge_code_for_product_template(product_template_id)
        wrapper = self.expected_opencell_configuration_wrapper
        wrapper.get_one_shot_charge_codes().get_code_for_product_template.assert_called_with(product_template_id)